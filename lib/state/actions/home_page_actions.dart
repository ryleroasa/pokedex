import 'dart:async';

import 'package:async_redux/async_redux.dart';
import 'package:pokedex_app/api/api_service.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/state/actions/actions.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/app_starter.dart';

const getPokemonListKey = 'get-pokemon-list-key';

/// Gets the Pokemon list displayed in the homepage and
/// save it in the state.
class GetPokemonListAction extends LoadingAction {
  GetPokemonListAction() : super(actionKey: getPokemonListKey);

  @override
  void before() {
    dispatch(ClearSearchedPokemons());
    super.before();
  }

  @override
  Future<AppState> reduce() async {
    final pokemonList = await getIt<ApiService>().pokemonDataApi.getPokemonList();

    return state.copyWith(pokemonList: pokemonList);
  }

  @override
  void after() {
    dispatch(ChangeFilterCancelVisibilityAction(isShown: false));
    super.after();
  }
}

/// Gets the filtered Pokemon list and save it in the state
class GetFilteredPokemonListAction extends LoadingAction {
  GetFilteredPokemonListAction({this.type}) : super(actionKey: getPokemonListKey);

  final String? type;

  @override
  void before() {
    dispatch(ClearSearchedPokemons());
    super.before();
  }

  @override
  Future<AppState> reduce() async {
    final pokemonList = await getIt<ApiService>().pokemonDataApi.getFilteredPokemonList(type!);

    return state.copyWith(pokemonList: pokemonList);
  }

  @override
  void after() {
    dispatch(ChangeFilterCancelVisibilityAction(isShown: true));
    super.after();
  }
}

/// Update the value of [isCancelFilterShown] in state whenever filtering Pokemons
class ChangeFilterCancelVisibilityAction extends ReduxAction<AppState> {
  ChangeFilterCancelVisibilityAction({this.isShown});

  final bool? isShown;

  @override
  AppState reduce() => state.copyWith(isCancelFilterShown: isShown);
}

/// Updates the value og [searchedPokemons] in state
class UpdateSearchedPokemons extends ReduxAction<AppState> {
  UpdateSearchedPokemons({required this.searchedPokemons});

  final List<Pokemon>? searchedPokemons;

  @override
  AppState reduce() => state.copyWith(searchedPokemons: searchedPokemons);
}

/// Clears the value of [searchedPokemons] in state
class ClearSearchedPokemons extends ReduxAction<AppState> {
  @override
  AppState reduce() => state.copyWith(searchedPokemons: null);
}
