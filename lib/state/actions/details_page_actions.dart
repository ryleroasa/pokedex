import 'package:pokedex_app/api/api_service.dart';
import 'package:pokedex_app/state/actions/actions.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/app_starter.dart';
import 'package:pokedex_app/utilities/constants.dart';

class GetPokemonTypesAction extends LoadingAction {
  GetPokemonTypesAction({this.pokemonId}) : super(actionKey: getPokemonTypesKey);

  final int? pokemonId;

  @override
  Future<AppState> reduce() async {
    final types = await getIt<ApiService>().pokemonDataApi.getPokemonTypes(pokemonId);

    return state.copyWith(types: types);
  }
}

/// Gets the Pokemon About details displayed under the About tab
/// and save it in the state.
class GetPokemonAboutAction extends LoadingAction {
  GetPokemonAboutAction({this.pokemonId}) : super(actionKey: getPokemonAboutKey);

  final int? pokemonId;

  @override
  Future<AppState> reduce() async {
    final aboutDetails = await getIt<ApiService>().pokemonDataApi.getPokemonAbout(pokemonId);

    return state.copyWith(aboutDetails: aboutDetails);
  }
}

class GetPokemonBaseStatsAction extends LoadingAction {
  GetPokemonBaseStatsAction({this.pokemonId}) : super(actionKey: getPokemonBaseStatsKey);

  final int? pokemonId;

  @override
  Future<AppState> reduce() async {
    final baseStats = await getIt<ApiService>().pokemonDataApi.getPokemonBaseStats(pokemonId);

    return state.copyWith(baseStats: baseStats);
  }
}

class GetPokemonMovesAction extends LoadingAction {
  GetPokemonMovesAction({this.pokemonId}) : super(actionKey: getPokemonMovesKey);

  final int? pokemonId;

  @override
  Future<AppState> reduce() async {
    final moves = await getIt<ApiService>().pokemonDataApi.getPokemonMoves(pokemonId);

    return state.copyWith(moves: moves);
  }
}

class GetPokemonEvolutions extends LoadingAction {
  GetPokemonEvolutions({this.pokemonId}) : super(actionKey: getPokemonEvolutionsKey);

  final int? pokemonId;

  @override
  Future<AppState> reduce() async {
    final evolutionId = await getIt<ApiService>().pokemonDataApi.getEvolutionChainId(pokemonId);
    final evolutions = await getIt<ApiService>().pokemonDataApi.getPokemonEvolutions(evolutionId);

    return state.copyWith(evolutions: evolutions);
  }
}
