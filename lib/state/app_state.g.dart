// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppState _$$_AppStateFromJson(Map<String, dynamic> json) => _$_AppState(
      pokemonList: (json['pokemon-list'] as List<dynamic>?)
          ?.map((e) => Pokemon.fromJson(e as Map<String, dynamic>))
          .toList(),
      types: (json['types'] as List<dynamic>?)
          ?.map((e) => PokemonType.fromJson(e as Map<String, dynamic>))
          .toList(),
      aboutDetails: json['about-details'] == null
          ? null
          : About.fromJson(json['about-details'] as Map<String, dynamic>),
      baseStats: (json['base-stats'] as List<dynamic>?)
          ?.map((e) => BaseStat.fromJson(e as Map<String, dynamic>))
          .toList(),
      moves: (json['moves'] as List<dynamic>?)
          ?.map((e) => PokemonMove.fromJson(e as Map<String, dynamic>))
          .toList(),
      evolutions: json['evolutions'] == null
          ? null
          : EvolutionChain.fromJson(json['evolutions'] as Map<String, dynamic>),
      searchedPokemons: (json['search-pokemons'] as List<dynamic>?)
          ?.map((e) => Pokemon.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AppStateToJson(_$_AppState instance) =>
    <String, dynamic>{
      'pokemon-list': instance.pokemonList,
      'types': instance.types,
      'about-details': instance.aboutDetails,
      'base-stats': instance.baseStats,
      'moves': instance.moves,
      'evolutions': instance.evolutions,
      'search-pokemons': instance.searchedPokemons,
    };
