import 'package:async_redux/async_redux.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pokedex_app/api/models/about_model.dart';
import 'package:pokedex_app/api/models/base_stat_model.dart';
import 'package:pokedex_app/api/models/evolution_chain.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/api/models/pokemon_move_model.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';

part 'app_state.freezed.dart';
part 'app_state.g.dart';

@freezed
class AppState with _$AppState {
  factory AppState({
    @JsonKey(name: 'pokemon-list') List<Pokemon>? pokemonList,
    @JsonKey(name: 'types') List<PokemonType>? types,
    @JsonKey(name: 'about-details') About? aboutDetails,
    @JsonKey(name: 'base-stats') List<BaseStat>? baseStats,
    @JsonKey(name: 'moves') List<PokemonMove>? moves,
    @JsonKey(name: 'evolutions') EvolutionChain? evolutions,
    @JsonKey(name: 'search-pokemons') List<Pokemon>? searchedPokemons,
    @Default(false) @JsonKey(name: 'is-cancel-filter-shown', ignore: true) bool? isCancelFilterShown,
    @Default(Wait.empty) @JsonKey(name: 'wait', ignore: true) Wait wait,
  }) = _AppState;
  factory AppState.init() => AppState(wait: Wait());
  factory AppState.fromJson(Map<String, dynamic> json) => _$AppStateFromJson(json);
}
