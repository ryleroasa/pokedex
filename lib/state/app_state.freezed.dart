// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'app_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppState _$AppStateFromJson(Map<String, dynamic> json) {
  return _AppState.fromJson(json);
}

/// @nodoc
class _$AppStateTearOff {
  const _$AppStateTearOff();

  _AppState call(
      {@JsonKey(name: 'pokemon-list')
          List<Pokemon>? pokemonList,
      @JsonKey(name: 'types')
          List<PokemonType>? types,
      @JsonKey(name: 'about-details')
          About? aboutDetails,
      @JsonKey(name: 'base-stats')
          List<BaseStat>? baseStats,
      @JsonKey(name: 'moves')
          List<PokemonMove>? moves,
      @JsonKey(name: 'evolutions')
          EvolutionChain? evolutions,
      @JsonKey(name: 'search-pokemons')
          List<Pokemon>? searchedPokemons,
      @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
          bool? isCancelFilterShown = false,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait = Wait.empty}) {
    return _AppState(
      pokemonList: pokemonList,
      types: types,
      aboutDetails: aboutDetails,
      baseStats: baseStats,
      moves: moves,
      evolutions: evolutions,
      searchedPokemons: searchedPokemons,
      isCancelFilterShown: isCancelFilterShown,
      wait: wait,
    );
  }

  AppState fromJson(Map<String, Object> json) {
    return AppState.fromJson(json);
  }
}

/// @nodoc
const $AppState = _$AppStateTearOff();

/// @nodoc
mixin _$AppState {
  @JsonKey(name: 'pokemon-list')
  List<Pokemon>? get pokemonList => throw _privateConstructorUsedError;
  @JsonKey(name: 'types')
  List<PokemonType>? get types => throw _privateConstructorUsedError;
  @JsonKey(name: 'about-details')
  About? get aboutDetails => throw _privateConstructorUsedError;
  @JsonKey(name: 'base-stats')
  List<BaseStat>? get baseStats => throw _privateConstructorUsedError;
  @JsonKey(name: 'moves')
  List<PokemonMove>? get moves => throw _privateConstructorUsedError;
  @JsonKey(name: 'evolutions')
  EvolutionChain? get evolutions => throw _privateConstructorUsedError;
  @JsonKey(name: 'search-pokemons')
  List<Pokemon>? get searchedPokemons => throw _privateConstructorUsedError;
  @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
  bool? get isCancelFilterShown => throw _privateConstructorUsedError;
  @JsonKey(name: 'wait', ignore: true)
  Wait get wait => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppStateCopyWith<AppState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppStateCopyWith<$Res> {
  factory $AppStateCopyWith(AppState value, $Res Function(AppState) then) =
      _$AppStateCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'pokemon-list')
          List<Pokemon>? pokemonList,
      @JsonKey(name: 'types')
          List<PokemonType>? types,
      @JsonKey(name: 'about-details')
          About? aboutDetails,
      @JsonKey(name: 'base-stats')
          List<BaseStat>? baseStats,
      @JsonKey(name: 'moves')
          List<PokemonMove>? moves,
      @JsonKey(name: 'evolutions')
          EvolutionChain? evolutions,
      @JsonKey(name: 'search-pokemons')
          List<Pokemon>? searchedPokemons,
      @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
          bool? isCancelFilterShown,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait});

  $AboutCopyWith<$Res>? get aboutDetails;
  $EvolutionChainCopyWith<$Res>? get evolutions;
}

/// @nodoc
class _$AppStateCopyWithImpl<$Res> implements $AppStateCopyWith<$Res> {
  _$AppStateCopyWithImpl(this._value, this._then);

  final AppState _value;
  // ignore: unused_field
  final $Res Function(AppState) _then;

  @override
  $Res call({
    Object? pokemonList = freezed,
    Object? types = freezed,
    Object? aboutDetails = freezed,
    Object? baseStats = freezed,
    Object? moves = freezed,
    Object? evolutions = freezed,
    Object? searchedPokemons = freezed,
    Object? isCancelFilterShown = freezed,
    Object? wait = freezed,
  }) {
    return _then(_value.copyWith(
      pokemonList: pokemonList == freezed
          ? _value.pokemonList
          : pokemonList // ignore: cast_nullable_to_non_nullable
              as List<Pokemon>?,
      types: types == freezed
          ? _value.types
          : types // ignore: cast_nullable_to_non_nullable
              as List<PokemonType>?,
      aboutDetails: aboutDetails == freezed
          ? _value.aboutDetails
          : aboutDetails // ignore: cast_nullable_to_non_nullable
              as About?,
      baseStats: baseStats == freezed
          ? _value.baseStats
          : baseStats // ignore: cast_nullable_to_non_nullable
              as List<BaseStat>?,
      moves: moves == freezed
          ? _value.moves
          : moves // ignore: cast_nullable_to_non_nullable
              as List<PokemonMove>?,
      evolutions: evolutions == freezed
          ? _value.evolutions
          : evolutions // ignore: cast_nullable_to_non_nullable
              as EvolutionChain?,
      searchedPokemons: searchedPokemons == freezed
          ? _value.searchedPokemons
          : searchedPokemons // ignore: cast_nullable_to_non_nullable
              as List<Pokemon>?,
      isCancelFilterShown: isCancelFilterShown == freezed
          ? _value.isCancelFilterShown
          : isCancelFilterShown // ignore: cast_nullable_to_non_nullable
              as bool?,
      wait: wait == freezed
          ? _value.wait
          : wait // ignore: cast_nullable_to_non_nullable
              as Wait,
    ));
  }

  @override
  $AboutCopyWith<$Res>? get aboutDetails {
    if (_value.aboutDetails == null) {
      return null;
    }

    return $AboutCopyWith<$Res>(_value.aboutDetails!, (value) {
      return _then(_value.copyWith(aboutDetails: value));
    });
  }

  @override
  $EvolutionChainCopyWith<$Res>? get evolutions {
    if (_value.evolutions == null) {
      return null;
    }

    return $EvolutionChainCopyWith<$Res>(_value.evolutions!, (value) {
      return _then(_value.copyWith(evolutions: value));
    });
  }
}

/// @nodoc
abstract class _$AppStateCopyWith<$Res> implements $AppStateCopyWith<$Res> {
  factory _$AppStateCopyWith(_AppState value, $Res Function(_AppState) then) =
      __$AppStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'pokemon-list')
          List<Pokemon>? pokemonList,
      @JsonKey(name: 'types')
          List<PokemonType>? types,
      @JsonKey(name: 'about-details')
          About? aboutDetails,
      @JsonKey(name: 'base-stats')
          List<BaseStat>? baseStats,
      @JsonKey(name: 'moves')
          List<PokemonMove>? moves,
      @JsonKey(name: 'evolutions')
          EvolutionChain? evolutions,
      @JsonKey(name: 'search-pokemons')
          List<Pokemon>? searchedPokemons,
      @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
          bool? isCancelFilterShown,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait});

  @override
  $AboutCopyWith<$Res>? get aboutDetails;
  @override
  $EvolutionChainCopyWith<$Res>? get evolutions;
}

/// @nodoc
class __$AppStateCopyWithImpl<$Res> extends _$AppStateCopyWithImpl<$Res>
    implements _$AppStateCopyWith<$Res> {
  __$AppStateCopyWithImpl(_AppState _value, $Res Function(_AppState) _then)
      : super(_value, (v) => _then(v as _AppState));

  @override
  _AppState get _value => super._value as _AppState;

  @override
  $Res call({
    Object? pokemonList = freezed,
    Object? types = freezed,
    Object? aboutDetails = freezed,
    Object? baseStats = freezed,
    Object? moves = freezed,
    Object? evolutions = freezed,
    Object? searchedPokemons = freezed,
    Object? isCancelFilterShown = freezed,
    Object? wait = freezed,
  }) {
    return _then(_AppState(
      pokemonList: pokemonList == freezed
          ? _value.pokemonList
          : pokemonList // ignore: cast_nullable_to_non_nullable
              as List<Pokemon>?,
      types: types == freezed
          ? _value.types
          : types // ignore: cast_nullable_to_non_nullable
              as List<PokemonType>?,
      aboutDetails: aboutDetails == freezed
          ? _value.aboutDetails
          : aboutDetails // ignore: cast_nullable_to_non_nullable
              as About?,
      baseStats: baseStats == freezed
          ? _value.baseStats
          : baseStats // ignore: cast_nullable_to_non_nullable
              as List<BaseStat>?,
      moves: moves == freezed
          ? _value.moves
          : moves // ignore: cast_nullable_to_non_nullable
              as List<PokemonMove>?,
      evolutions: evolutions == freezed
          ? _value.evolutions
          : evolutions // ignore: cast_nullable_to_non_nullable
              as EvolutionChain?,
      searchedPokemons: searchedPokemons == freezed
          ? _value.searchedPokemons
          : searchedPokemons // ignore: cast_nullable_to_non_nullable
              as List<Pokemon>?,
      isCancelFilterShown: isCancelFilterShown == freezed
          ? _value.isCancelFilterShown
          : isCancelFilterShown // ignore: cast_nullable_to_non_nullable
              as bool?,
      wait: wait == freezed
          ? _value.wait
          : wait // ignore: cast_nullable_to_non_nullable
              as Wait,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AppState implements _AppState {
  _$_AppState(
      {@JsonKey(name: 'pokemon-list')
          this.pokemonList,
      @JsonKey(name: 'types')
          this.types,
      @JsonKey(name: 'about-details')
          this.aboutDetails,
      @JsonKey(name: 'base-stats')
          this.baseStats,
      @JsonKey(name: 'moves')
          this.moves,
      @JsonKey(name: 'evolutions')
          this.evolutions,
      @JsonKey(name: 'search-pokemons')
          this.searchedPokemons,
      @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
          this.isCancelFilterShown = false,
      @JsonKey(name: 'wait', ignore: true)
          this.wait = Wait.empty});

  factory _$_AppState.fromJson(Map<String, dynamic> json) =>
      _$$_AppStateFromJson(json);

  @override
  @JsonKey(name: 'pokemon-list')
  final List<Pokemon>? pokemonList;
  @override
  @JsonKey(name: 'types')
  final List<PokemonType>? types;
  @override
  @JsonKey(name: 'about-details')
  final About? aboutDetails;
  @override
  @JsonKey(name: 'base-stats')
  final List<BaseStat>? baseStats;
  @override
  @JsonKey(name: 'moves')
  final List<PokemonMove>? moves;
  @override
  @JsonKey(name: 'evolutions')
  final EvolutionChain? evolutions;
  @override
  @JsonKey(name: 'search-pokemons')
  final List<Pokemon>? searchedPokemons;
  @override
  @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
  final bool? isCancelFilterShown;
  @override
  @JsonKey(name: 'wait', ignore: true)
  final Wait wait;

  @override
  String toString() {
    return 'AppState(pokemonList: $pokemonList, types: $types, aboutDetails: $aboutDetails, baseStats: $baseStats, moves: $moves, evolutions: $evolutions, searchedPokemons: $searchedPokemons, isCancelFilterShown: $isCancelFilterShown, wait: $wait)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AppState &&
            (identical(other.pokemonList, pokemonList) ||
                const DeepCollectionEquality()
                    .equals(other.pokemonList, pokemonList)) &&
            (identical(other.types, types) ||
                const DeepCollectionEquality().equals(other.types, types)) &&
            (identical(other.aboutDetails, aboutDetails) ||
                const DeepCollectionEquality()
                    .equals(other.aboutDetails, aboutDetails)) &&
            (identical(other.baseStats, baseStats) ||
                const DeepCollectionEquality()
                    .equals(other.baseStats, baseStats)) &&
            (identical(other.moves, moves) ||
                const DeepCollectionEquality().equals(other.moves, moves)) &&
            (identical(other.evolutions, evolutions) ||
                const DeepCollectionEquality()
                    .equals(other.evolutions, evolutions)) &&
            (identical(other.searchedPokemons, searchedPokemons) ||
                const DeepCollectionEquality()
                    .equals(other.searchedPokemons, searchedPokemons)) &&
            (identical(other.isCancelFilterShown, isCancelFilterShown) ||
                const DeepCollectionEquality()
                    .equals(other.isCancelFilterShown, isCancelFilterShown)) &&
            (identical(other.wait, wait) ||
                const DeepCollectionEquality().equals(other.wait, wait)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(pokemonList) ^
      const DeepCollectionEquality().hash(types) ^
      const DeepCollectionEquality().hash(aboutDetails) ^
      const DeepCollectionEquality().hash(baseStats) ^
      const DeepCollectionEquality().hash(moves) ^
      const DeepCollectionEquality().hash(evolutions) ^
      const DeepCollectionEquality().hash(searchedPokemons) ^
      const DeepCollectionEquality().hash(isCancelFilterShown) ^
      const DeepCollectionEquality().hash(wait);

  @JsonKey(ignore: true)
  @override
  _$AppStateCopyWith<_AppState> get copyWith =>
      __$AppStateCopyWithImpl<_AppState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppStateToJson(this);
  }
}

abstract class _AppState implements AppState {
  factory _AppState(
      {@JsonKey(name: 'pokemon-list')
          List<Pokemon>? pokemonList,
      @JsonKey(name: 'types')
          List<PokemonType>? types,
      @JsonKey(name: 'about-details')
          About? aboutDetails,
      @JsonKey(name: 'base-stats')
          List<BaseStat>? baseStats,
      @JsonKey(name: 'moves')
          List<PokemonMove>? moves,
      @JsonKey(name: 'evolutions')
          EvolutionChain? evolutions,
      @JsonKey(name: 'search-pokemons')
          List<Pokemon>? searchedPokemons,
      @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
          bool? isCancelFilterShown,
      @JsonKey(name: 'wait', ignore: true)
          Wait wait}) = _$_AppState;

  factory _AppState.fromJson(Map<String, dynamic> json) = _$_AppState.fromJson;

  @override
  @JsonKey(name: 'pokemon-list')
  List<Pokemon>? get pokemonList => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'types')
  List<PokemonType>? get types => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'about-details')
  About? get aboutDetails => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'base-stats')
  List<BaseStat>? get baseStats => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'moves')
  List<PokemonMove>? get moves => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'evolutions')
  EvolutionChain? get evolutions => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'search-pokemons')
  List<Pokemon>? get searchedPokemons => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'is-cancel-filter-shown', ignore: true)
  bool? get isCancelFilterShown => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'wait', ignore: true)
  Wait get wait => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$AppStateCopyWith<_AppState> get copyWith =>
      throw _privateConstructorUsedError;
}
