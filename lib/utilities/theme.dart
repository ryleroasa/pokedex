import 'package:flutter/material.dart';
import 'package:pokedex_app/utilities/colors.dart';

final ThemeData theme = ThemeData(
  textTheme: TextTheme(
    headline1: TextStyle(
      color: white,
      fontWeight: FontWeight.bold,
      fontSize: 24,
    ),
    headline2: TextStyle(
      color: white,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    ),
    headline3: TextStyle(
      color: white,
      fontSize: 12,
    ),
    bodyText1: TextStyle(
      color: black,
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
  ),
);
