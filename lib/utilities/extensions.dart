import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/utilities/colors.dart';

extension StringExtension on String {
  Color get typeColor {
    switch (this) {
      case 'fire':
        return fire;
      case 'grass':
        return grass;
      case 'water':
        return water;
      case 'normal':
        return normal;
      case 'flying':
        return flying;
      case 'bug':
        return bug;
      case 'poison':
        return poison;
      case 'ground':
        return ground;
      case 'electric':
        return electric;
      case 'fighting':
        return fighting;
      case 'psychic':
        return psychic;
      case 'rock':
        return rock;
      case 'ice':
        return ice;
      case 'ghost':
        return ghost;
      case 'dragon':
        return dragon;
      case 'dark':
        return dark;
      case 'steel':
        return steel;
      case 'fairy':
        return fairy;
      default:
        return white;
    }
  }

  String get capitalized => '${this[0].toUpperCase()}${substring(1).toLowerCase()}';

  String get addLeadingZeros => "#${padLeft(4, '0')}";

  String get formattedName => replaceDash.split(' ').map((string) => string.capitalized).join(' ');

  String get removeParenthesis => replaceAll(')', '').replaceAll('(', '');

  String get replaceDash => replaceAll('-', ' ');

  String get toBaseExp => '$this xp';

  int get pokemonId => int.parse(Uri.parse(this).pathSegments.fourth);
}

extension IntExt on int {
  String get convertToHeight => '${(this / 10).toString()} m';

  String get convertToWeight => '${(this / 10).toString()} kg';
}

extension ListStringExt on List<String> {
  String get capitalizedAbilities => map((ability) => ability.capitalized).toString().removeParenthesis.replaceDash;
}
