import 'package:flutter/material.dart';

const fire = Color(0xffe66062);
const grass = Color(0xff48b59b);
const water = Color(0xff68a6d9);
const normal = Color(0xff949066);
const flying = Color(0xff44807b);
const bug = Color(0xff799931);
const poison = Color(0xffa763c2);
const ground = Color(0xff7a7517);
const electric = Color(0xff998928);
const fighting = Color(0xffcf8f8c);
const psychic = Color(0xffc74a78);
const rock = Color(0xff695c3a);
const ice = Color(0xff519fad);
const ghost = Color(0xff663f75);
const dragon = Color(0xffaa9bc9);
const dark = Color(0xff4f4f4f);
const steel = Color(0xff5d6b68);
const fairy = Color(0xffc9819b);

const white = Color(0xffffffff);
const black = Colors.black;
const red = Colors.redAccent;
const orange = Colors.orangeAccent;

Color? darkGrey = Colors.grey[900];
Color? lighterGrey = Colors.grey[800];
Color? transparent = Colors.white.withOpacity(0.2);
Color? whiteTransparent = Colors.white70;
