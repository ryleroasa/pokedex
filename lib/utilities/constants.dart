const String baseUrl = 'https://pokeapi.co/api/v2/';

const String iconUrl =
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/';

const getPokemonTypesKey = 'get-pokemon-types-key';
const getPokemonAboutKey = 'get-pokemon-about-key';
const getPokemonBaseStatsKey = 'get-pokemon-base-stats-key';
const getPokemonMovesKey = 'get-pokemon-moves-key';
const getPokemonEvolutionsKey = 'get-pokemon-evolutions-key';

const String height = 'Height';
const String weight = 'Weight';
const String abilities = 'Abilities';
const String baseExperience = 'Base Experience';

const stats = ['HP', 'Attack', 'Defense', 'Sp-Atk', 'Sp-Def', 'Speed'];
const tabs = ['About', 'Base Stats', 'Evolution', 'Moves'];

const List<String> types = [
  'fire',
  'grass',
  'water',
  'normal',
  'flying',
  'bug',
  'poison',
  'ground',
  'electric',
  'fighting',
  'psychic',
  'rock',
  'ice',
  'ghost',
  'dragon',
  'dark',
  'steel',
  'fairy',
];

// Image size constants
const double imageHeight = 100;
const double imageWidth = 100;
const double detailsPageImageHeight = 250;
const double detailsPageImageWidth = 250;

// Hero tags
const heroTagCancel = 'cancel';
const heroTagFilter = 'filter';
