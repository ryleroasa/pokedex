import 'package:dartx/dartx.dart';
import 'package:pokedex_app/api/api_client.dart';
import 'package:pokedex_app/api/models/about_model.dart';
import 'package:pokedex_app/api/models/base_stat_model.dart';
import 'package:pokedex_app/api/models/evolution_chain.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/api/models/pokemon_move_model.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';

class PokemonDataApi {
  PokemonDataApi(ApiClient this.apiClient);

  final ApiClient apiClient;

  Future<List<Pokemon>> getPokemonList() async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(
      path: baseUri.path + 'pokemon',
      query: 'limit=1154',
    );

    return await apiClient.dio
        .getUri(uri)
        .then((response) => (response.data['results'] as List).map((pokemon) => Pokemon.fromJson(pokemon)).toList());
  }

  Future<List<Pokemon>> getFilteredPokemonList(String type) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'type/$type');

    return await apiClient.dio.getUri(uri).then((response) =>
        (response.data['pokemon'] as List).map((pokemon) => Pokemon.fromJson(pokemon['pokemon'])).toList());
  }

  Future<List<PokemonType>> getPokemonTypes(int? id) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'pokemon/$id');

    return await apiClient.dio.getUri(uri).then(
        (response) => (response.data['types'] as List).map((type) => PokemonType.fromJson(type['type'])).toList());
  }

  Future<About?> getPokemonAbout(int? id) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'pokemon/$id');

    return await apiClient.dio.getUri(uri).then((response) => About.fromJson(response.data));
  }

  Future<List<BaseStat>?> getPokemonBaseStats(int? id) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'pokemon/$id');

    return await apiClient.dio
        .getUri(uri)
        .then((response) => (response.data['stats'] as List).map((stat) => BaseStat.fromJson(stat)).toList());
  }

  Future<List<PokemonMove>?> getPokemonMoves(int? id) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'pokemon/$id');

    return await apiClient.dio.getUri(uri).then(
        (response) => (response.data['moves'] as List).map((move) => PokemonMove.fromJson(move['move'])).toList());
  }

  Future<int?> getEvolutionChainId(int? id) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'pokemon-species/$id');

    final evolutionUrl = await apiClient.dio.getUri(uri).then((response) => response.data['evolution_chain']['url']);

    return int.parse(Uri.parse(evolutionUrl).pathSegments.fourth);
  }

  Future<EvolutionChain?> getPokemonEvolutions(int? evolutionId) async {
    final baseUri = Uri.parse(apiClient.baseUrl);
    final uri = baseUri.replace(path: baseUri.path + 'evolution-chain/$evolutionId');

    return await apiClient.dio.getUri(uri).then((response) {
      final results = response.data['chain'];

      final firstEvolutionData = results['species'];
      final secondEvolutionData = results['evolves_to'];
      final hasSecondEvolutions = secondEvolutionData.isNotEmpty;
      final hasThirdEvolutions =
          (secondEvolutionData as List).map((evolution) => evolution['evolves_to'].isNotEmpty).all((e) => e == true);

      late List<Pokemon>? secondEvolutions;
      late List<Pokemon>? thirdEvolutions;

      if (hasSecondEvolutions) {
        secondEvolutions = secondEvolutionData.map((evolution) => Pokemon.fromJson(evolution['species'])).toList();
      }

      if (hasThirdEvolutions) {
        final evolutionList = secondEvolutionData.map((e) => e['evolves_to']);
        final sortedList = evolutionList
            .map((e) => e.map((s) => [s['species']['name'], s['species']['url']]))
            .expand((e) => e)
            .toSet()
            .toList();

        thirdEvolutions = sortedList
            .map((evolution) => Pokemon(
                  name: evolution[0],
                  url: evolution[1],
                ))
            .toList();
      }

      return EvolutionChain(
        firstEvolution: Pokemon.fromJson(firstEvolutionData),
        secondEvolutions: hasSecondEvolutions ? secondEvolutions : <Pokemon>[],
        thirdEvolutions: hasThirdEvolutions ? thirdEvolutions : <Pokemon>[],
      );
    });
  }
}
