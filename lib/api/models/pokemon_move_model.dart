import 'package:freezed_annotation/freezed_annotation.dart';

part 'pokemon_move_model.freezed.dart';
part 'pokemon_move_model.g.dart';

@freezed
class PokemonMove with _$PokemonMove {
  factory PokemonMove({
    @JsonKey(name: 'name') String? name,
  }) = _PokemonMove;

  factory PokemonMove.fromJson(Map<String, dynamic> json) => _$PokemonMoveFromJson(json);
}
