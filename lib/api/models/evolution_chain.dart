import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';

part 'evolution_chain.freezed.dart';
part 'evolution_chain.g.dart';

@freezed
class EvolutionChain with _$EvolutionChain {
  factory EvolutionChain({
    @JsonKey(name: 'first-evolution') Pokemon? firstEvolution,
    @JsonKey(name: 'second-evolutions') List<Pokemon>? secondEvolutions,
    @JsonKey(name: 'third-evolutions') List<Pokemon>? thirdEvolutions,
  }) = _Evolutions;

  factory EvolutionChain.fromJson(Map<String, dynamic> json) => _$EvolutionChainFromJson(json);
}
