import 'package:freezed_annotation/freezed_annotation.dart';

part 'base_stat_model.freezed.dart';
part 'base_stat_model.g.dart';

@freezed
class BaseStat with _$BaseStat {
  factory BaseStat({
    @JsonKey(name: 'base_stat') int? baseStat,
  }) = _BaseStat;

  factory BaseStat.fromJson(Map<String, dynamic> json) => _$BaseStatFromJson(json);
}
