import 'package:pokedex_app/api/api_client.dart';
import 'package:pokedex_app/api/handlers/pokemon_data_handler.dart';
import 'package:pokedex_app/utilities/constants.dart';

class ApiService {
  PokemonDataApi get pokemonDataApi => _pokemonDataApi ??= PokemonDataApi(_pokemonDataAPIClient);

  ApiClient get _pokemonDataAPIClient => _createApiClient(baseUrl: baseUrl);

  ApiClient _createApiClient({required String baseUrl}) => ApiClient(baseUrl: baseUrl);

  PokemonDataApi? _pokemonDataApi;
}
