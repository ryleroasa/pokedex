import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class BaseStatsRow extends StatelessWidget {
  const BaseStatsRow({
    this.label,
    this.value,
    this.primaryType,
  });

  final String? label;
  final int? value;
  final PokemonType? primaryType;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      width: 400,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(
              label ?? '',
              style: textTheme.bodyText1,
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              (value ?? 0).toString(),
              style: textTheme.bodyText1,
            ),
          ),
          Expanded(
            flex: 4,
            child: LinearPercentIndicator(
              progressColor: primaryType?.name?.typeColor,
              backgroundColor: Colors.grey[200],
              barRadius: const Radius.circular(5),
              percent: (value! / 255),
            ),
          )
        ],
      ),
    );
  }
}
