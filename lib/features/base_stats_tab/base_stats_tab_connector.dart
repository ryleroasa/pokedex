import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/base_stats_tab/base_stats_tab.dart';
import 'package:pokedex_app/features/base_stats_tab/base_stats_vm.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/state/actions/details_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';

class BaseStatsTabConnector extends StatelessWidget {
  const BaseStatsTabConnector({
    this.pokemonId,
    this.primarytype,
  });

  final int? pokemonId;
  final PokemonType? primarytype;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, BaseStatsTabVm>(
      vm: () => BaseStatsTabVmFactory(),
      onInit: (vm) => vm.dispatch(GetPokemonBaseStatsAction(pokemonId: pokemonId)),
      builder: (context, vm) => vm.pageState.when(
        (baseStats) => BaseStatsTab(
          baseStats: baseStats,
          primaryType: primarytype,
        ),
        loading: () => const Center(child: LoadingWidget()),
        error: (errorMessage) => Center(child: Text(errorMessage!)),
      ),
    );
  }
}
