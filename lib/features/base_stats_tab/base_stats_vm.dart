import 'package:async_redux/async_redux.dart';
import 'package:pokedex_app/api/models/base_stat_model.dart';
import 'package:pokedex_app/features/base_stats_tab/base_stats_tab_connector.dart';
import 'package:pokedex_app/models/union_page_state.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/constants.dart';

class BaseStatsTabVmFactory extends VmFactory<AppState, BaseStatsTabConnector> {
  @override
  Vm fromStore() => BaseStatsTabVm(
        baseStats: state.baseStats,
        pageState: _getPageState(),
      );

  UnionPageState<List<BaseStat>> _getPageState() {
    if (state.wait.isWaitingFor(getPokemonBaseStatsKey)) {
      return const UnionPageState.loading();
    } else if (state.baseStats != null) {
      return UnionPageState(state.baseStats);
    } else {
      return const UnionPageState.error('Unable to get Base Stats Data');
    }
  }
}

class BaseStatsTabVm extends Vm {
  BaseStatsTabVm({
    required this.baseStats,
    required this.pageState,
  }) : super(equals: [baseStats, pageState]);

  final List<BaseStat>? baseStats;

  late final UnionPageState<List<BaseStat>> pageState;
}
