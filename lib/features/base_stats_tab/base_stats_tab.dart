import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/base_stat_model.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/base_stats_tab/widgets/base_stats_row.dart';
import 'package:pokedex_app/utilities/constants.dart';

class BaseStatsTab extends StatelessWidget {
  const BaseStatsTab({
    required this.baseStats,
    required this.primaryType,
  });

  final List<BaseStat>? baseStats;
  final PokemonType? primaryType;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ...?baseStats?.mapIndexed(
            (index, stat) => BaseStatsRow(
              label: stats[index],
              value: stat.baseStat,
              primaryType: primaryType,
            ),
          ),
        ],
      ),
    );
  }
}
