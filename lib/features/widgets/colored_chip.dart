import 'package:flutter/material.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class ColoredChip extends StatelessWidget {
  const ColoredChip({
    required this.name,
    required this.color,
  });

  final String? name;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 6,
      ),
      margin: const EdgeInsets.only(
        right: 2,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(
        name!.capitalized,
        style: textTheme.headline3,
      ),
    );
  }
}
