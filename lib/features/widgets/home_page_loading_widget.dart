import 'package:flutter/material.dart';
import 'package:pokedex_app/features/home_page/widgets/search_pokemons.dart';
import 'package:pokedex_app/utilities/string_constants.dart';

class HomePageLoadingWidget extends StatelessWidget {
  const HomePageLoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(title),
            SearchPokemons(
              onChanged: (_) {},
              hintText: '',
            ),
          ],
        ),
        backgroundColor: Colors.grey[900],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orangeAccent,
        onPressed: () {},
        child: const Icon(Icons.filter_list),
      ),
      backgroundColor: Colors.grey[800],
      body: const Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
          color: Colors.orangeAccent,
        ),
      ),
    );
  }
}
