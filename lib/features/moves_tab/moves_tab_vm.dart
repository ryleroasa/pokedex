import 'package:async_redux/async_redux.dart';
import 'package:pokedex_app/api/models/pokemon_move_model.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/moves_tab/moves_tab_connector.dart';
import 'package:pokedex_app/models/union_page_state.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/constants.dart';

class MovesTabVmFactory extends VmFactory<AppState, MovesTabConnector> {
  @override
  Vm fromStore() => MovesTabVm(
        moves: state.moves,
        primaryType: state.types?.first,
        pageState: _getPageState(),
      );

  UnionPageState<List<PokemonMove>> _getPageState() {
    if (state.wait.isWaitingFor(getPokemonMovesKey)) {
      return const UnionPageState.loading();
    } else if (state.pokemonList?.isNotEmpty == true) {
      return UnionPageState(state.moves);
    } else {
      return const UnionPageState.error('Unable to get Moves Data');
    }
  }
}

class MovesTabVm extends Vm {
  MovesTabVm({
    required this.moves,
    required this.primaryType,
    required this.pageState,
  }) : super(equals: [moves, primaryType, pageState]);

  final List<PokemonMove>? moves;
  final PokemonType? primaryType;

  late final UnionPageState<List<PokemonMove>> pageState;
}
