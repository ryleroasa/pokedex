import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/features/moves_tab/moves_tab.dart';
import 'package:pokedex_app/features/moves_tab/moves_tab_vm.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/state/actions/details_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';

class MovesTabConnector extends StatelessWidget {
  const MovesTabConnector({this.pokemonId});

  final int? pokemonId;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MovesTabVm>(
      vm: () => MovesTabVmFactory(),
      onInit: (vm) => vm.dispatch(GetPokemonMovesAction(pokemonId: pokemonId)),
      builder: (context, vm) => vm.pageState.when(
        (moves) => MovesTab(
          moves: moves,
          primaryType: vm.primaryType,
        ),
        loading: () => const Center(child: LoadingWidget()),
        error: (errorMessage) => Center(child: Text(errorMessage!)),
      ),
    );
  }
}
