import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_move_model.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/widgets/colored_chip.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class MovesTab extends StatelessWidget {
  const MovesTab({
    this.moves,
    this.primaryType,
  });

  final List<PokemonMove>? moves;
  final PokemonType? primaryType;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(24),
        child: Wrap(
          children: moves
                  ?.map((move) => ColoredChip(
                        name: move.name,
                        color: primaryType?.name?.typeColor,
                      ))
                  .toList() ??
              [],
        ),
      ),
    );
  }
}
