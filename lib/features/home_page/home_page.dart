import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/features/details_page/details_page_connector.dart';
import 'package:pokedex_app/features/home_page/widgets/filter_modal.dart';
import 'package:pokedex_app/features/home_page/widgets/pokemon_tile.dart';
import 'package:pokedex_app/features/home_page/widgets/search_pokemons.dart';
import 'package:pokedex_app/utilities/colors.dart';
import 'package:pokedex_app/utilities/constants.dart';
import 'package:pokedex_app/utilities/extensions.dart';
import 'package:pokedex_app/utilities/string_constants.dart';

class HomePage extends StatelessWidget {
  const HomePage({
    required this.onPressed,
    required this.onClearFilter,
    required this.onChanged,
    required this.pokemonList,
    required this.isCancelFilterShown,
  });

  final Function(String type) onPressed;
  final VoidCallback onClearFilter;
  final ValueChanged<String> onChanged;
  final List<Pokemon>? pokemonList;
  final bool isCancelFilterShown;

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    final textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: textTheme.headline1,
            ),
            SearchPokemons(
              onChanged: onChanged,
              hintText: '',
            ),
          ],
        ),
        backgroundColor: darkGrey,
      ),
      backgroundColor: lighterGrey,
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          if (isCancelFilterShown)
            FloatingActionButton(
              backgroundColor: red,
              onPressed: onClearFilter,
              heroTag: heroTagCancel,
              child: const Icon(Icons.highlight_remove_sharp),
            ),
          const SizedBox(width: 16),
          FloatingActionButton(
            backgroundColor: orange,
            onPressed: () => showModalBottomSheet(
              context: context,
              backgroundColor: darkGrey,
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(25))),
              builder: (BuildContext context) => FilterModal(onPressed: onPressed),
            ),
            heroTag: heroTagFilter,
            child: const Icon(Icons.filter_list),
          ),
        ],
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: isPortrait ? 2 : 5),
        itemCount: pokemonList?.length,
        itemBuilder: (context, index) {
          return (pokemonList != null)
              ? PokemonTile(
                  onPressed: () => Navigator.pushNamed(
                    context,
                    DetailsPageConnector.route,
                    arguments: DetailsPageConnectorArgs(pokemon: pokemonList?[index]),
                  ),
                  name: pokemonList?[index].name?.capitalized,
                  id: pokemonList?[index].url?.pokemonId,
                  url: pokemonList?[index].url,
                  key: ValueKey(pokemonList?[index].name),
                )
              : const Spacer();
        },
      ),
    );
  }
}
