import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/api_service.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/widgets/colored_chip.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/utilities/app_starter.dart';
import 'package:pokedex_app/utilities/colors.dart';
import 'package:pokedex_app/utilities/constants.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class PokemonTile extends StatefulWidget {
  const PokemonTile({
    required this.onPressed,
    required this.name,
    required this.url,
    required this.id,
    Key? key,
  }) : super(key: key);

  final VoidCallback onPressed;
  final String? name;
  final String? url;
  final int? id;

  @override
  State<PokemonTile> createState() => _PokemonTileState();
}

class _PokemonTileState extends State<PokemonTile> {
  List<PokemonType> pokemonTypes = [];
  bool isLoading = true;

  @override
  void initState() {
    getPokemonTypes();
    super.initState();
  }

  void getPokemonTypes() async {
    final typesList = await getIt<ApiService>().pokemonDataApi.getPokemonTypes(widget.id);
    pokemonTypes = typesList;

    if (mounted) {
      isLoading = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return GestureDetector(
      onTap: widget.onPressed,
      child: Container(
        margin: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: (pokemonTypes.isNotEmpty) ? pokemonTypes.first.name?.typeColor : Colors.grey[700],
          borderRadius: BorderRadius.circular(24),
        ),
        padding: const EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 10,
          right: 5,
        ),
        child: isLoading
            ? const LoadingWidget()
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.name?.formattedName ?? '',
                    style: textTheme.headline2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ...pokemonTypes.map((type) => ColoredChip(
                                name: type.name,
                                color: transparent,
                              )),
                        ],
                      ),
                      if (widget.id != null)
                        CachedNetworkImage(
                          imageUrl: '$iconUrl${widget.id}.png',
                          height: imageHeight,
                          width: imageWidth,
                          placeholder: (context, url) => const LoadingWidget(),
                          errorWidget: (context, url, error) => const Icon(Icons.error),
                        ),
                    ],
                  ),
                ],
              ),
      ),
    );
  }
}
