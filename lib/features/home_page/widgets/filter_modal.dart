import 'package:flutter/material.dart';
import 'package:pokedex_app/features/widgets/colored_chip.dart';
import 'package:pokedex_app/utilities/constants.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class FilterModal extends StatelessWidget {
  FilterModal({required this.onPressed});

  final Function(String name) onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(18),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: [
          ...types.map((type) => GestureDetector(
                onTap: () {
                  onPressed(type);
                  Navigator.pop(context);
                },
                child: ColoredChip(
                  name: type,
                  color: type.typeColor,
                ),
              ))
        ],
      ),
    );
  }
}
