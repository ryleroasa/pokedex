import 'package:flutter/material.dart';

class SearchPokemons extends StatefulWidget {
  const SearchPokemons({
    required this.onChanged,
    required this.hintText,
  });

  final ValueChanged<String> onChanged;
  final String hintText;

  @override
  State<SearchPokemons> createState() => _SearchPokemonsState();
}

class _SearchPokemonsState extends State<SearchPokemons> {
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      width: 200,
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(25),
      ),
      child: TextField(
        controller: controller,
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.orangeAccent,
        decoration: InputDecoration(
          icon: const Icon(
            Icons.search,
            color: Colors.white,
          ),
          border: InputBorder.none,
          suffixIcon: controller.text.isNotEmpty
              ? GestureDetector(
                  child: const Icon(
                    Icons.close,
                    color: Colors.orangeAccent,
                  ),
                  onTap: () {
                    controller.clear();
                    widget.onChanged('');
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                )
              : null,
          hintText: widget.hintText,
        ),
        onChanged: widget.onChanged,
      ),
    );
  }
}
