import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/features/home_page/home_page_connector.dart';
import 'package:pokedex_app/models/union_page_state.dart';
import 'package:pokedex_app/state/actions/home_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class HomePageVmFactory extends VmFactory<AppState, HomePageConnector> {
  @override
  Vm fromStore() => HomePageVm(
        isCancelFilterShown: state.isCancelFilterShown,
        pageState: _getPageState(),
        onPressed: _onFilterPokemons,
        onClearFilter: _onClearFilter,
        onChanged: _onSearchPokemons,
      );

  void _onFilterPokemons(type) => dispatch(GetFilteredPokemonListAction(type: type));

  void _onClearFilter() => dispatch(GetPokemonListAction());

  void _onSearchPokemons(String query) {
    final pokemons = state.pokemonList?.where((pokemon) {
      final nameToLowerCase = pokemon.name?.toLowerCase();
      final idToString = pokemon.url?.pokemonId.toString();
      final queryToLowerCase = query.toLowerCase();

      return nameToLowerCase!.contains(queryToLowerCase) || idToString!.contains(queryToLowerCase);
    }).toList();

    dispatch(UpdateSearchedPokemons(searchedPokemons: pokemons));
  }

  UnionPageState<List<Pokemon>> _getPageState() {
    if (state.wait.isWaitingFor(getPokemonListKey)) {
      return const UnionPageState.loading();
    } else if (state.searchedPokemons?.isNotEmpty == true) {
      return UnionPageState(state.searchedPokemons);
    } else if (state.searchedPokemons?.isEmpty == true) {
      return UnionPageState([]);
    } else if (state.pokemonList?.isNotEmpty == true) {
      return UnionPageState(state.pokemonList);
    } else {
      return const UnionPageState.error('Home Page Error Message');
    }
  }
}

class HomePageVm extends Vm {
  HomePageVm({
    required this.pageState,
    required this.onPressed,
    required this.onChanged,
    required this.onClearFilter,
    required this.isCancelFilterShown,
  }) : super(equals: [isCancelFilterShown, pageState]);

  final Function(String type) onPressed;
  final ValueChanged<String> onChanged;
  final VoidCallback onClearFilter;
  final bool? isCancelFilterShown;
  late final UnionPageState<List<Pokemon>> pageState;
}
