import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/features/home_page/home_page.dart';
import 'package:pokedex_app/features/home_page/home_page_vm.dart';
import 'package:pokedex_app/features/widgets/home_page_loading_widget.dart';
import 'package:pokedex_app/state/actions/home_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';

class HomePageConnector extends StatelessWidget {
  const HomePageConnector();

  static const String route = 'home-page-connector';

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomePageVm>(
      vm: () => HomePageVmFactory(),
      onInit: (vm) => vm.dispatch(GetPokemonListAction()),
      builder: (context, vm) => vm.pageState.when(
        (pokemonList) => HomePage(
          pokemonList: pokemonList,
          onPressed: vm.onPressed,
          onChanged: vm.onChanged,
          onClearFilter: vm.onClearFilter,
          isCancelFilterShown: vm.isCancelFilterShown ?? false,
        ),
        loading: () => const Center(child: HomePageLoadingWidget()),
        error: (errorMessage) => Center(child: Text(errorMessage!)),
      ),
    );
  }
}
