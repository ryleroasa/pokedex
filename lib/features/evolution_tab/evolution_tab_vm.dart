import 'package:async_redux/async_redux.dart';
import 'package:pokedex_app/api/models/evolution_chain.dart';
import 'package:pokedex_app/features/evolution_tab/evolution_tab_connector.dart';
import 'package:pokedex_app/models/union_page_state.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/constants.dart';

class EvolutionTabVmFactory extends VmFactory<AppState, EvolutionTabConnector> {
  @override
  Vm fromStore() => EvolutionTabVm(
        evolutions: state.evolutions,
        pageState: _getPageState(),
      );

  UnionPageState<EvolutionChain> _getPageState() {
    if (state.wait.isWaitingFor(getPokemonEvolutionsKey)) {
      return const UnionPageState.loading();
    } else if (state.evolutions != null) {
      return UnionPageState(state.evolutions);
    } else {
      return const UnionPageState.error('Unable to get Evolution Data');
    }
  }
}

class EvolutionTabVm extends Vm {
  EvolutionTabVm({
    required this.evolutions,
    required this.pageState,
  }) : super(equals: [evolutions, pageState]);

  final EvolutionChain? evolutions;

  late final UnionPageState<EvolutionChain> pageState;
}
