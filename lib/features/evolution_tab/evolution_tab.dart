import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/evolution_chain.dart';
import 'package:pokedex_app/features/evolution_tab/widgets/evolution_column.dart';
import 'package:pokedex_app/features/evolution_tab/widgets/evolution_tile.dart';

class EvolutionTab extends StatelessWidget {
  const EvolutionTab({required this.evolutions});

  final EvolutionChain? evolutions;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (evolutions?.firstEvolution != null)
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  EvolutionTile(
                    evolution: evolutions?.firstEvolution,
                    isFirstEvolution: true,
                  )
                ],
              )
            : Center(child: Text('No evolutions')),
        if (evolutions?.secondEvolutions?.isNotEmpty == true)
          EvolutionColumn(evolutions: evolutions?.secondEvolutions ?? []),
        if (evolutions?.thirdEvolutions?.isNotEmpty == true) ...[
          EvolutionColumn(evolutions: evolutions?.thirdEvolutions ?? []),
        ],
      ],
    );
  }
}
