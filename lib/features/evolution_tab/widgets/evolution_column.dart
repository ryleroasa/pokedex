import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/features/evolution_tab/widgets/evolution_tile.dart';

class EvolutionColumn extends StatelessWidget {
  const EvolutionColumn({required this.evolutions});

  final List<Pokemon> evolutions;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          if (evolutions.isNotEmpty) ...[
            ...evolutions.map((evolution) => EvolutionTile(
                  evolution: evolution,
                  isFirstEvolution: false,
                ))
          ]
        ],
      ),
    );
  }
}
