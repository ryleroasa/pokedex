import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/utilities/constants.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class EvolutionTile extends StatelessWidget {
  const EvolutionTile({
    required this.evolution,
    required this.isFirstEvolution,
  });

  final Pokemon? evolution;
  final bool isFirstEvolution;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Row(
      children: [
        if (!isFirstEvolution) const Icon(Icons.arrow_forward_ios_rounded),
        Column(
          children: [
            CachedNetworkImage(
              imageUrl: '$iconUrl${evolution?.url?.pokemonId}.png',
              height: imageHeight,
              width: imageWidth,
              placeholder: (context, url) => const LoadingWidget(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
            const SizedBox(height: 6),
            Text(
              evolution?.name?.formattedName ?? '',
              style: textTheme.bodyText1,
            ),
          ],
        ),
      ],
    );
  }
}
