import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/features/evolution_tab/evolution_tab.dart';
import 'package:pokedex_app/features/evolution_tab/evolution_tab_vm.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/state/actions/details_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';

class EvolutionTabConnector extends StatelessWidget {
  const EvolutionTabConnector({this.pokemonId});

  final int? pokemonId;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, EvolutionTabVm>(
      vm: () => EvolutionTabVmFactory(),
      onInit: (vm) => vm.dispatch(GetPokemonEvolutions(pokemonId: pokemonId)),
      builder: (context, vm) => vm.pageState.when(
        (evolutions) => EvolutionTab(evolutions: evolutions),
        loading: () => const Center(child: LoadingWidget()),
        error: (errorMessage) => Center(child: Text(errorMessage!)),
      ),
    );
  }
}
