import 'package:flutter/material.dart';

class AboutRow extends StatelessWidget {
  const AboutRow({
    this.label,
    this.value,
  });

  final String? label;
  final String? value;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Text(
              label ?? '',
              style: textTheme.bodyText1,
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              value ?? '',
              style: textTheme.bodyText1,
            ),
          ),
        ],
      ),
    );
  }
}
