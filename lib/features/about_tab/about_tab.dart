import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/about_model.dart';
import 'package:pokedex_app/features/about_tab/widgets/about_row.dart';
import 'package:pokedex_app/utilities/constants.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class AboutTab extends StatelessWidget {
  const AboutTab({required this.aboutDetails});

  final About? aboutDetails;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: Column(
        children: [
          AboutRow(
            label: height,
            value: aboutDetails?.height?.convertToHeight,
          ),
          AboutRow(
            label: weight,
            value: aboutDetails?.weight?.convertToWeight,
          ),
          AboutRow(
            label: abilities,
            value: aboutDetails?.abilities
                ?.map((ability) => ability['ability']['name'].toString())
                .toList()
                .capitalizedAbilities,
          ),
          AboutRow(
            label: baseExperience,
            value: aboutDetails?.baseExperience.toString().toBaseExp,
          ),
        ],
      ),
    );
  }
}
