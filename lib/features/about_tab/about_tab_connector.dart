import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/features/about_tab/about_tab.dart';
import 'package:pokedex_app/features/about_tab/about_tab_vm.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/state/actions/details_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';

class AboutTabConnector extends StatelessWidget {
  const AboutTabConnector({this.pokemonId});

  final int? pokemonId;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AboutTabVm>(
      vm: () => AboutTabVmFactory(),
      onInit: (vm) => vm.dispatch(GetPokemonAboutAction(pokemonId: pokemonId)),
      builder: (context, vm) => vm.pageState.when(
        (aboutDetails) => AboutTab(aboutDetails: aboutDetails),
        loading: () => const Center(child: LoadingWidget()),
        error: (errorMessage) => Center(child: Text(errorMessage!)),
      ),
    );
  }
}
