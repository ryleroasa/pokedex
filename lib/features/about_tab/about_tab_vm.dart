import 'package:async_redux/async_redux.dart';
import 'package:pokedex_app/api/models/about_model.dart';
import 'package:pokedex_app/features/about_tab/about_tab_connector.dart';
import 'package:pokedex_app/models/union_page_state.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/constants.dart';

class AboutTabVmFactory extends VmFactory<AppState, AboutTabConnector> {
  @override
  Vm fromStore() => AboutTabVm(
        aboutDetails: state.aboutDetails,
        pageState: _getPageState(),
      );

  UnionPageState<About> _getPageState() {
    if (state.wait.isWaitingFor(getPokemonAboutKey)) {
      return const UnionPageState.loading();
    } else if (state.aboutDetails != null) {
      return UnionPageState(state.aboutDetails);
    } else {
      return const UnionPageState.error('Unable to get About Data');
    }
  }
}

class AboutTabVm extends Vm {
  AboutTabVm({
    required this.aboutDetails,
    required this.pageState,
  }) : super(equals: [aboutDetails, pageState]);

  final About? aboutDetails;

  late final UnionPageState<About> pageState;
}
