import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/about_tab/about_tab_connector.dart';
import 'package:pokedex_app/features/base_stats_tab/base_stats_tab_connector.dart';
import 'package:pokedex_app/features/evolution_tab/evolution_tab_connector.dart';
import 'package:pokedex_app/features/moves_tab/moves_tab_connector.dart';
import 'package:pokedex_app/features/widgets/colored_chip.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/utilities/colors.dart';
import 'package:pokedex_app/utilities/constants.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({
    this.pokemon,
    this.types,
  });

  final Pokemon? pokemon;
  final List<PokemonType>? types;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final pokemonId = pokemon?.url?.pokemonId;

    return Scaffold(
      backgroundColor: types?.first.name?.typeColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(
                Icons.arrow_back,
                color: white,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    pokemon?.name?.formattedName ?? '',
                    style: textTheme.headline1,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      ...?types?.map((type) => ColoredChip(
                            name: type.name,
                            color: transparent,
                          )),
                      const Spacer(),
                      Text(
                        pokemon?.url?.pokemonId.toString().addLeadingZeros ?? '',
                        style: textTheme.headline2,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Center(
              child: CachedNetworkImage(
                imageUrl: '$iconUrl${pokemonId}.png',
                height: detailsPageImageHeight,
                width: detailsPageImageWidth,
                placeholder: (context, url) => const LoadingWidget(),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: whiteTransparent,
                  borderRadius: BorderRadius.vertical(top: Radius.circular(25)),
                ),
                child: DefaultTabController(
                  length: tabs.length,
                  initialIndex: 0,
                  child: Column(
                    children: [
                      TabBar(
                        tabs: [...tabs.map((title) => Tab(text: title))],
                        labelColor: black,
                        indicatorColor: orange,
                      ),
                      Expanded(
                        child: TabBarView(
                          children: [
                            AboutTabConnector(pokemonId: pokemonId),
                            BaseStatsTabConnector(
                              pokemonId: pokemonId,
                              primarytype: types?.first,
                            ),
                            EvolutionTabConnector(pokemonId: pokemonId),
                            MovesTabConnector(pokemonId: pokemonId),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
