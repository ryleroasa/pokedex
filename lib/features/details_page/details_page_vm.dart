import 'package:async_redux/async_redux.dart';
import 'package:pokedex_app/api/models/pokemon_type_model.dart';
import 'package:pokedex_app/features/details_page/details_page_connector.dart';
import 'package:pokedex_app/models/union_page_state.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/constants.dart';

class DetailsPageVmFactory extends VmFactory<AppState, DetailsPageConnector> {
  @override
  Vm fromStore() => DetailsPageVm(
        types: state.types,
        pageState: _getPageState(),
      );

  UnionPageState<List<PokemonType>> _getPageState() {
    if (state.wait.isWaitingFor(getPokemonTypesKey)) {
      return const UnionPageState.loading();
    } else if (state.types?.isNotEmpty == true) {
      return UnionPageState(state.types);
    } else {
      return const UnionPageState.error('Unable to get Pokemon details');
    }
  }
}

class DetailsPageVm extends Vm {
  DetailsPageVm({
    required this.types,
    required this.pageState,
  }) : super(equals: [types, pageState]);

  final List<PokemonType>? types;

  late final UnionPageState<List<PokemonType>> pageState;
}
