import 'package:async_redux/async_redux.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/models/pokemon_model.dart';
import 'package:pokedex_app/features/details_page/details_page.dart';
import 'package:pokedex_app/features/details_page/details_page_vm.dart';
import 'package:pokedex_app/features/widgets/loading_widget.dart';
import 'package:pokedex_app/state/actions/details_page_actions.dart';
import 'package:pokedex_app/state/app_state.dart';
import 'package:pokedex_app/utilities/extensions.dart';

class DetailsPageConnectorArgs {
  const DetailsPageConnectorArgs({this.pokemon});

  final Pokemon? pokemon;
}

class DetailsPageConnector extends StatelessWidget {
  const DetailsPageConnector({required this.args});

  static const String route = 'details-page-connector';

  final DetailsPageConnectorArgs args;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DetailsPageVm>(
      vm: () => DetailsPageVmFactory(),
      onInit: (vm) => vm.dispatch(GetPokemonTypesAction(pokemonId: args.pokemon?.url?.pokemonId)),
      builder: (context, vm) => vm.pageState.when(
        (types) => DetailsPage(
          pokemon: args.pokemon,
          types: types,
        ),
        loading: () => const Center(child: LoadingWidget()),
        error: (errorMessage) => Center(child: Text(errorMessage!)),
      ),
    );
  }
}
